import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private usersRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto): Promise<Product> {
    const product: Product = new Product();

    product.name = createProductDto.name;
    product.price = createProductDto.price;
    return this.usersRepository.save(product);
  }

  findAll(): Promise<Product[]> {
    return this.usersRepository.find();
  }

  findOne(id: number): Promise<Product> {
    return this.usersRepository.findOneBy({ id: id });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.usersRepository.findOneBy({ id: id });
    const updatedProducts = { ...product, ...updateProductDto };
    return this.usersRepository.save(updatedProducts);
  }

  async remove(id: number) {
    const product = await this.usersRepository.findOneBy({ id: id });

    return this.usersRepository.remove(product);
  }
}
